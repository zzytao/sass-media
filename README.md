#针对scss的媒体查询自适应函数

<ul>
	<li>phone -> 手机</li>
	<li>iPhone4 -> iPhone4</li>
	<li>iPhone5 -> iPhone5</li>
	<li>iPhone6 -> iPhone6 & iPhone7</li>
	<li>plus -> iPhone6 plus & iPhone7 plus</li>
	<li>ipad -> ipad</li>
	<li>s-pc -> 笔记本</li>
	<li>l-pc -> 1080P显示器</li>
	<li>xl-pc -> 大于1080P显示器</li>
</ul>

###直接在需要做自适应的位置引入:

<pre>
<code>
	@include zsy('需要适配的设备'){
		'适配的scss';
	}
</code>
</pre>

